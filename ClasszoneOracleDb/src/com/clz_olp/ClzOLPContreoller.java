package com.clz_olp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClzOLPContreoller {
	DbConnection dbConnection;

	public ClzOLPContreoller() {
		dbConnection = new DbConnection();
	}

	public List<ClasszoneOLP> getUsersDetails(String line) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "  select lp.TItle as \"Lesson Plan Title\", l.TItle as \"Lesson Title\",  l.objectives as \"Lesson Objective\", a.Full_title as \"Activity Title\", a.feature, a.subsection, a.unit, a.part, a.Page, a. filename as \"PDF\",\r\n" + 
					"  a.selection_id, a.spanish_translation as \"Spanish Version\", a.spanish_answers as \"Spanish AsnwerSheet\", a.answers, a.answers_filename, s.selection, s.objectives, anc.ancillary_code, anc.ancillary_book, bs.title as \"Book\", bs.book_id\r\n" + 
					"  from activities a, book_selections bs, ancillary_titles anc, selections s, lesson l, lesson_plan lp, lesson_activity la\r\n" + 
					"  where a.book_id = bs.book_id\r\n" + 
					"  -- match up user's Lesson PLan to Lesson  (on Plan_ID), Lesson to Lesson_Activity (compositve key Activity_ID & Lesson_ID)\r\n" + 
					"  and lp.plan_id = l.plan_id and l.lesson_id = la.lesson_id and la.activity_id = a.activity_id\r\n" + 
					"  -- match up Ancillary Codes - that table has a composite PK of Book ID + Ancillary Code\r\n" + 
					"  and (a.book_id = anc.book_id\r\n" + 
					"  and anc.ancillary_code = a.ancillary_code)\r\n" + 
					"  -- match up selections - this table also has a composite PK of Book ID + Selection ID\r\n" + 
					"  and (a.book_id = s.book_id\r\n" + 
					"  and s.selection_id = a.selection_id)\r\n" + 
					"  and lp.web_userid =" + line + " \r\n" + 
					"    order by unit, part";

			con = this.dbConnection.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			List<ClasszoneOLP> listClszone = new ArrayList<ClasszoneOLP>();
			while (rs.next()) {
				ClasszoneOLP classzoneOLP = new ClasszoneOLP();
				classzoneOLP.setLessonPlanTitle(rs.getString("Lesson Plan Title"));
				classzoneOLP.setLessonTitle(rs.getString("Lesson Title"));
				classzoneOLP.setLessonObjective(rs.getString("Lesson Objective"));
				classzoneOLP.setActivityTitle(rs.getString("Activity Title"));
				classzoneOLP.setFeature(rs.getString("FEATURE"));
				classzoneOLP.setSubsection(rs.getString("SUBSECTION"));
				classzoneOLP.setUnit(rs.getString("UNIT"));
				classzoneOLP.setPart(rs.getString("PART"));
				classzoneOLP.setPage(rs.getString("PAGE"));
				classzoneOLP.setPdf(rs.getString("PDF"));
				classzoneOLP.setSelectionId(rs.getString("SELECTION_ID"));
				classzoneOLP.setSpanishVersion(rs.getString("Spanish Version"));
				classzoneOLP.setSpanishAsnwerSheet(rs.getString("Spanish AsnwerSheet"));
				classzoneOLP.setAnswers(rs.getString("ANSWERS"));
				classzoneOLP.setAnswersFilename(rs.getString("ANSWERS_FILENAME"));
				classzoneOLP.setSelection(rs.getString("SELECTION"));
				classzoneOLP.setObjectives(rs.getString("OBJECTIVES"));
				classzoneOLP.setAncillaryCode(rs.getString("ANCILLARY_CODE"));
				classzoneOLP.setAncillaryBook(rs.getString("ANCILLARY_BOOK"));
				classzoneOLP.setBook(rs.getString("Book"));
				classzoneOLP.setBookId(rs.getString("BOOK_ID"));
				listClszone.add(classzoneOLP);
			}
			return listClszone;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
}
