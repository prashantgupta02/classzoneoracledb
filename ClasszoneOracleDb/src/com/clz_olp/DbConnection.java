package com.clz_olp;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {
	
	private final String CONNECTION_URL = "jdbc:oracle:thin:@10.34.42.69:1521:orcl11g";
	private final String USERNAME = "XXXX";
	private final String PASSWORD = "XXXX";
	private final String DRIVERNAME = "oracle.jdbc.driver.OracleDriver";

	public Connection getConnection() throws Exception {
		Class.forName(DRIVERNAME);
		return DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);

	}

}
