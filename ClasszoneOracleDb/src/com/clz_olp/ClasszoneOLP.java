package com.clz_olp;

import java.io.Serializable;

public class ClasszoneOLP implements Serializable {
	private static final long serialVersionUID = 1L;

	private String lessonPlanTitle;
	private String lessonTitle;
	private String lessonObjective;
	private String activityTitle;
	private String feature;
	private String subsection;
	private String unit;
	private String part;
	private String page;
	private String pdf;
	private String selectionId;
	private String spanishVersion;
	private String spanishAsnwerSheet;
	private String answers;
	private String answersFilename;
	private String selection;
	private String objectives;
	private String ancillaryCode;
	private String ancillaryBook;
	private String book;
	private String bookId;

	private final String[] columnsArra = new String[] { "Lesson Plan Title", "Lesson Title", "Lesson Objective",
			"Activity Title", "FEATURE", "SUBSECTION", "UNIT", "PART", "PAGE", "PDF", "SELECTION_ID", "Spanish Version",
			"Spanish AsnwerSheet", "ANSWERS", "ANSWERS_FILENAME", "SELECTION", "OBJECTIVES", "ANCILLARY_CODE", "ANCILLARY_BOOK", "Book", "BOOK_ID" };

	public String getLessonPlanTitle() {
		return lessonPlanTitle;
	}

	public void setLessonPlanTitle(String lessonPlanTitle) {
		this.lessonPlanTitle = lessonPlanTitle;
	}

	public String getLessonTitle() {
		return lessonTitle;
	}

	public void setLessonTitle(String lessonTitle) {
		this.lessonTitle = lessonTitle;
	}

	public String getLessonObjective() {
		return lessonObjective;
	}

	public void setLessonObjective(String lessonObjective) {
		this.lessonObjective = lessonObjective;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getSubsection() {
		return subsection;
	}

	public void setSubsection(String subsection) {
		this.subsection = subsection;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public String getSelectionId() {
		return selectionId;
	}

	public void setSelectionId(String selectionId) {
		this.selectionId = selectionId;
	}

	public String getSpanishVersion() {
		return spanishVersion;
	}

	public void setSpanishVersion(String spanishVersion) {
		this.spanishVersion = spanishVersion;
	}

	public String getSpanishAsnwerSheet() {
		return spanishAsnwerSheet;
	}

	public void setSpanishAsnwerSheet(String spanishAsnwerSheet) {
		this.spanishAsnwerSheet = spanishAsnwerSheet;
	}
	
	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}
	public String getAnswersFilename() {
		return answersFilename;
	}

	public void setAnswersFilename(String answersFilename) {
		this.answersFilename = answersFilename;
	}

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	public String getOjectives() {
		return objectives;
	}

	public void setObjectives(String objectives) {
		this.objectives = objectives;
	}

	public String getAncillaryCode() {
		return ancillaryCode;
	}

	public void setAncillaryCode(String ancillaryCode) {
		this.ancillaryCode = ancillaryCode;
	}

	public String getAncillaryBook() {
		return ancillaryBook;
	}

	public void setAncillaryBook(String ancillaryBook) {
		this.ancillaryBook = ancillaryBook;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String[] getColumnsArra() {
		return columnsArra;
	}

}
