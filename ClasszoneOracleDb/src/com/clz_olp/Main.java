package com.clz_olp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Main {
	public static void main(String args[]) throws Exception {
		Main main = new Main();
		ClzOLPContreoller clzOLPContreoller = new ClzOLPContreoller();
		System.out.println("Please Enter File Path::::");

		String input = new Scanner(System.in).nextLine();
		if (input == null || input.trim().length() == 0) {
			System.out.println("Please Enter File Path::::");
			input = new Scanner(System.in).nextLine();
		}
		String regEx4Win = "\\\\(?=[^\\\\]+$)";
		String path = input.split(regEx4Win)[0];
		
		try (BufferedReader br = new BufferedReader(new FileReader(input))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (line != null && line.trim().length() > 0) {
					try {
						List<ClasszoneOLP> list = clzOLPContreoller.getUsersDetails(line);
						main.createExcelSheet(list, line, path);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				br.readLine();
			}
		}

	}

	private void createExcelSheet(List<ClasszoneOLP> list, String fileName, String path) {
		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		// Create a Sheet name
		Sheet sheet = workbook.createSheet("UserDetails");

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 10);

		// Create a Row
		Row headerRow = sheet.createRow(0);
		String[] columns = new ClasszoneOLP().getColumnsArra();
		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		// Create Other rows and cells with employees data
		int rowNum = 1;
		for (ClasszoneOLP classzoneOLP : list) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(classzoneOLP.getLessonPlanTitle());
			row.createCell(1).setCellValue(classzoneOLP.getLessonTitle());
			row.createCell(2).setCellValue(classzoneOLP.getLessonObjective());
			row.createCell(3).setCellValue(classzoneOLP.getActivityTitle());
			row.createCell(4).setCellValue(classzoneOLP.getFeature());
			row.createCell(5).setCellValue(classzoneOLP.getSubsection());
			row.createCell(6).setCellValue(classzoneOLP.getUnit());
			row.createCell(7).setCellValue(classzoneOLP.getPart());
			row.createCell(8).setCellValue(classzoneOLP.getPage());
			row.createCell(9).setCellValue(classzoneOLP.getPdf());
			row.createCell(10).setCellValue(classzoneOLP.getSelectionId());
			row.createCell(11).setCellValue(classzoneOLP.getSpanishVersion());
			row.createCell(12).setCellValue(classzoneOLP.getSpanishAsnwerSheet());
			row.createCell(13).setCellValue(classzoneOLP.getAnswers());
			row.createCell(14).setCellValue(classzoneOLP.getAnswersFilename());
			row.createCell(15).setCellValue(classzoneOLP.getSelection());
			row.createCell(16).setCellValue(classzoneOLP.getOjectives());
			row.createCell(17).setCellValue(classzoneOLP.getAncillaryCode());
			row.createCell(18).setCellValue(classzoneOLP.getAncillaryBook());
			row.createCell(19).setCellValue(classzoneOLP.getBook());
			row.createCell(20).setCellValue(classzoneOLP.getBookId());
		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		// Write the output to a file
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(path +"\\"+ fileName + ".xlsx");
			try {
				workbook.write(fileOut);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				fileOut.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// Closing the workbook
			try {
				workbook.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
System.out.println("###############Completed###########################");
	}
}
